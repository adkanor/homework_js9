// 1.Создать тег на странице можно через  createElement(elementName)
// 2.insertAdjacentHTML(position, text) (position - значит позиция елемента относительно того елемента, для которого вызывалась функция )
// Можно расположить елемент относительно 4ех позиций, до открывающего тега елемента, после открывающего тега елемента, до и после закрывающего тега елемента.
// 3. Удаление елемента = использовать метод remove()

let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function listFromArray(arr, parent) {
  for (let value of arr) {
    let listItem = document.createElement("li");
    listItem.textContent = value;
    parent.append(listItem);
  }
}

console.log(listFromArray(array, document.body));

let timerShow = document.getElementById("timer");
let seconds = 3;
function countdown() {
  timerShow.innerHTML = seconds;
  seconds--;
  if (seconds < 0) {
    document.body.style.display = "none";
  } else {
    timer = setTimeout(countdown, 1000);
  }
}
countdown();
